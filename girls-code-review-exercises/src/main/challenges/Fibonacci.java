package challenges;

public class Fibonacci {
	
	public static long getFibonacciElement(int inFibonacciPosition) {
		if(isNegative(inFibonacciPosition)) {
			throw new IllegalArgumentException("Element's position cannot be negative");
		}
		
		if(isZeroOrFirstPosition(inFibonacciPosition)) {
			return 1L;
		}

		long currFibonacciElement = 1L;
		long prevFibonacciElement = 1L;
		long fibonaciiElement = 0L;
		for(int i = 2; i <= inFibonacciPosition; i++) {
			fibonaciiElement = prevFibonacciElement + currFibonacciElement;
			prevFibonacciElement = currFibonacciElement;
			currFibonacciElement = fibonaciiElement;
		}
		
		return fibonaciiElement;
	}

	private static boolean isNegative(int inFibonacciPosition) {
		return inFibonacciPosition < 0;
	}
	
	private static boolean isZeroOrFirstPosition(int inFibonacciPosition) {
		return inFibonacciPosition == 0 || inFibonacciPosition == 1;
	}

}

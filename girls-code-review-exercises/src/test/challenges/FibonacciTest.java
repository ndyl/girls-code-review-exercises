package challenges;

import static org.junit.Assert.*;

import org.junit.Test;

public class FibonacciTest {
	
	@Test(expected = IllegalArgumentException.class)
	public void getFibonacciElement_givenNegativeInt_ShouldThrowIllegalArgumentException() {
		int given = -1;
		Fibonacci.getFibonacciElement(given);
	}

	@Test
	public void getFibonacciElement_given5_ShouldReturn8() {
		int given = 5;
		long result = Fibonacci.getFibonacciElement(given);
		long expected = 8L;
		assertEquals(expected, result);
	}
	
	@Test
	public void getFibonacciElement_given30_ShouldReturn1346269() {
		int given = 30;
		long result = Fibonacci.getFibonacciElement(given);
		long expected = 1346269L;
		assertEquals(expected, result);
	}
	
	@Test
	public void getFibonacciElement_given0_ShouldReturn1() {
		int given = 0;
		long result = Fibonacci.getFibonacciElement(given);
		long expected = 1L;
		assertEquals(expected, result);
	}
	
	@Test
	public void getFibonacciElement_given1_ShouldReturn1() {
		int given = 1;
		long result = Fibonacci.getFibonacciElement(given);
		long expected = 1L;
		assertEquals(expected, result);
	}
}
